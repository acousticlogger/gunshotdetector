/****************************************************************************
 * BURTC_registers.h
 * Peter Prince
 * University of Southampton
 *****************************************************************************/

#ifndef BURTC_REGISTERS_H_
#define BURTC_REGISTERS_H_

#define BUR_BURT_COUNTER                        0
#define BUR_CLOCK_SET_FLAG                      1
#define BUR_DAY_REC_COUNTER                     2
#define BUR_COUNTER_RESET_DAY                   3
#define BUR_WATCH_DOG_FLAG                      4

#endif /* BURTC_REGISTERS_H_ */
