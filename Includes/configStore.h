/*
 * configStore.h
 *
 *  Created on: 7 Mar 2017
 *      Author: Andrew
 */

#ifndef CONFIGSTORE_H_
#define CONFIGSTORE_H_

#include <stdint.h>
#include "nonvolatile_flash.h"

#define CONFIG_CAPACITY 		1024
#define CONFIG_STRUCT_LENGTH 	(CONFIG_CAPACITY/4)

void ConfigStore_writeData(uint32_t *data);

uint32_t ConfigStore_getConfigWord(int32_t address);

#endif /* CONFIGSTORE_H_ */
