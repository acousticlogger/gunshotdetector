/****************************************************************************
 * gunshot_detector.h
 * Peter Prince
 * University of Southampton
 *****************************************************************************/

#ifndef GUNSHOT_DETECTOR_H
#define GUNSHOT_DETECTOR_H

typedef enum {
    RESPONSE_CONTINUE_SAMPLING,
    RESPONSE_START_RECORDING,
} gunshotDetectorResponse_t;


int gunshotDetector(int16_t * buffer1, int16_t * buffer2);

float32_t getDetectorValue();

float32_t getTestValue();


#endif
