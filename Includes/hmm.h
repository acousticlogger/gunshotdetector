/****************************************************************************
 * hmm.h
 * Peter Prince
 * University of Southampton
 *****************************************************************************/

#ifndef  hmm_def_INC
#define  hmm_def_INC

#include <arm_math.h>

float32_t normalpdf(float32_t X, float32_t mu, float32_t p1, float32_t variance);
float32_t calculate(float32_t freq1[], float32_t freq2[], float32_t freq3[], int T);

#endif
