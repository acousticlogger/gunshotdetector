/*
 * nonvolatile_flash.h
 *
 *  Created on: 6 Mar 2017
 *      Author: Andrew
 */

#ifndef NONVOLATILE_FLASH_H_
#define NONVOLATILE_FLASH_H_

#define HAMMING_FACTOR_LENGTH					128
#define DAY_BLOCKS								5

#define FLASH_SAMPLE_RATE					    0
#define FLASH_RECORDING_LENGTH					1
#define FLASH_SLEEP_LENGTH						2
#define FLASH_LISTEN_LENGTH                     3
#define FLASH_TRIGGER_FREQUENCY					4
#define FLASH_TRIGGER_THRESHOLD					5
#define FLASH_TRIGGER_BANDWIDTH					6
#define FLASH_TRIGGER_W							7

#define FLASH_NUMBER_OF_RECORDING_BLOCKS		8

#define FLASH_TRIGGER_ENABLED                   9
#define FLASH_GAIN						        10
#define FLASH_BATTERY_LIFE                      11
#define FLASH_LOG_FILE_ENABLED                  12

#define FLASH_RECORDING_START_TIME_H_1		    13
#define FLASH_RECORDING_START_TIME_M_1		    14
#define FLASH_RECORDING_END_TIME_H_1		    15
#define FLASH_RECORDING_END_TIME_M_1		    16

#define FLASH_RECORDING_START_TIME_H_2		    17
#define FLASH_RECORDING_START_TIME_M_2		    18
#define FLASH_RECORDING_END_TIME_H_2		    19
#define FLASH_RECORDING_END_TIME_M_2		    20

#define FLASH_RECORDING_START_TIME_H_3		    21
#define FLASH_RECORDING_START_TIME_M_3		    22
#define FLASH_RECORDING_END_TIME_H_3		    23
#define FLASH_RECORDING_END_TIME_M_3		    24

#define FLASH_RECORDING_START_TIME_H_4		    25
#define FLASH_RECORDING_START_TIME_M_4		    26
#define FLASH_RECORDING_END_TIME_H_4		    27
#define FLASH_RECORDING_END_TIME_M_4		   	28

#define FLASH_RECORDING_START_TIME_H_5		    29
#define FLASH_RECORDING_START_TIME_M_5		    30
#define FLASH_RECORDING_END_TIME_H_5		    31
#define FLASH_RECORDING_END_TIME_M_5		   	32

#define FLASH_RECORDING_START_DATE              29
#define FLASH_RECORDING_END_DATE               	30

#define FLASH_LED_ENABLED					    31

#define FLASH_HAMMING_FACTOR                    32

/** Wav header data structure */
typedef struct
{
    uint32_t sample_rate;
    uint32_t recording_length;
    uint32_t sleep_length;
    uint32_t listen_length;
    uint32_t trigger_frequency;
    uint32_t trigger_threshold;
    uint32_t trigger_bandwidth;
    uint32_t trigger_w;
    uint32_t no_recording_blocks;
    uint32_t recording_start_time_h[DAY_BLOCKS];
    uint32_t recording_end_time_h[DAY_BLOCKS];
    uint32_t recording_start_time_m[DAY_BLOCKS];
    uint32_t recording_end_time_m[DAY_BLOCKS];
    uint32_t recording_start_date;
    uint32_t recording_end_date;
    uint32_t trigger_enable;
    uint32_t gain;
    uint32_t battery_life;
    uint32_t log_file_enabled;
    uint32_t led_enabled;
    uint32_t hamming_factors[HAMMING_FACTOR_LENGTH];

} Flash_Data_TypeDef;

#endif /* NONVOLATILE_FLASH_H_ */
