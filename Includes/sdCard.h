/****************************************************************************
 * sdCard.h
 * Andrew Hill
 * University of Southampton
 *****************************************************************************/

#ifndef SDCARD_H_
#define SDCARD_H_

#include <stdint.h>

//#define SDWRITE_SIZE					    15000
#define SAMPLE_RATE                         8000                      				  /* Sample rate 192KHz*/
#define RECORD_TIME_SEC						5
#define ADCSAMPLES                          (1024*64)                  			      /* ADC samples to shift after each DMA transfer*/
#define BUFFERSIZE                          (1024*3)                  			      /* ADC samples to shift after each DMA transfer*/
#define FILE_SIZE                           (SAMPLE_RATE*2*RECORD_TIME_SEC)           /* Size of Each WAV file recording */
#define NUMBER_SDWRITES                     (FILE_SIZE/ADCSAMPLES)
#define ADC_DMA_SAMPLES                     1024                 			          /* ADC samples to shift after each DMA transfer*/
#define SRAM_BUFFERSIZE                     (1024*8)                  			      /* Size of one SRAM buffer (x2) */
#define NUMBER_OF_BUFFERS                   3

#define TIME_EXPANSION_MODE					0  /* 1 = time expansion mode, 0 = Normal Mode*/

/** Wav header data structure */
typedef struct
{
    uint8_t  id[4];                     /* should always contain "RIFF"      */
    uint32_t totallength;               /* total file length minus 8         */
    uint8_t  wavefmt[8];                /* should be "WAVEfmt "              */
    uint32_t format;                    /* Sample format. 16 for PCM format. */
    uint16_t pcm;                       /* 1 for PCM format                  */
    uint16_t channels;                  /* Channels                          */
    uint32_t frequency;                 /* sampling frequency                */
    uint32_t bytes_per_second;          /* Bytes per second                  */
    uint16_t bytes_per_capture;         /* Bytes per capture                 */
    uint16_t bits_per_sample;           /* Bits per sample                   */
    uint8_t  data[4];                   /* should always contain "data"      */
    uint32_t bytes_in_data;             /* No. bytes in data                 */
} WAV_Header_TypeDef;


static WAV_Header_TypeDef wavHeader = { /* Global Wav header is used in callbacks. */
    .id = "RIFF",
    .totallength = 0,
    .wavefmt = "WAVEfmt ",
    .format = 16,
    .pcm = 1,
    .channels = 1,
    /* Time expansion */
#if TIME_EXPANSION_MODE
    .frequency = 48000,
    .bytes_per_second = 48000 * 2,
#else
    .frequency = 0,
    .bytes_per_second = 0,
#endif
    .bytes_per_capture = 2,
    .bits_per_sample = 16,
    .data = "data",
    .bytes_in_data = 0
};

#endif /* SDCARD_H_ */
