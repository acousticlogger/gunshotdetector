/****************************************************************************
 * usb_requests.h
 * Peter Prince
 * University of Southampton
 *****************************************************************************/

#ifndef USB_REQUESTS_H_
#define USB_REQUESTS_H_

#include <stdint.h>
#include "BURTC_registers.h"
#include "em_burtc.h"

uint32_t getTimeRequestFromUSB();
void setTimeRequestFromUSB(uint32_t timeNow);

#endif /* USB_REQUESTS_H_ */
