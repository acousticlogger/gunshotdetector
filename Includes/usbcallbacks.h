/****************************************************************************
 * callbacks.h
 * Alex Rogers
 * University of Oxford
 *****************************************************************************/

#ifndef _USBCALLBACKS_H_
#define _USBCALLBACKS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "arm_math.h"

int setupCmd(const USB_Setup_TypeDef *setup);
void stateChange(USBD_State_TypeDef oldState, USBD_State_TypeDef newState);
int dataReceivedCallback(USB_Status_TypeDef status, uint32_t xferred, uint32_t remaining);
int dataSentCallback(USB_Status_TypeDef status, uint32_t xferred, uint32_t remaining);

#ifdef __cplusplus
}
#endif

#endif /* _USBCALLBACKS_H_ */
