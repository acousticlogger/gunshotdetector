/*
 * configStore.h
 *
 *  Created on: 7 Mar 2017
 *      Author: Andrew
 */
#include "configStore.h"

#include "em_msc.h"
#include "em_usb.h"
#include "em_device.h"

#define MIN(a,b) (((a)<(b))?(a):(b))

#define CONFIG_CAPACITY 		1024
#define CONFIG_STRUCT_LENGTH 	(CONFIG_CAPACITY/4)
#define CONFIG_ADDRESS	 		0x0FE00000
#define CONFIG_LENGTH_ADDRESS	0x0FE00400

uint32_t ConfigStore_getConfigWord(int32_t address) {

	return *((volatile uint32_t*) (CONFIG_ADDRESS + (address * 4)));


}


void ConfigStore_writeData(uint32_t *data) {

	/* Initialise for flash writing */

    MSC_Init();

    /* Set up pointers */

    uint32_t *flashConfigAddress = (uint32_t*)(CONFIG_ADDRESS);

    /* Erase and write flash */

    MSC_ErasePage(flashConfigAddress);

    MSC_WriteWord(flashConfigAddress, data, CONFIG_CAPACITY);

    /* Read back values*/

    MSC_Deinit();



}



