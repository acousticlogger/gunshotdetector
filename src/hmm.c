/****************************************************************************
 * hmm.c
 * Peter Prince
 * University of Southampton
 *****************************************************************************/

#include "hmm.h"
#include <arm_math.h>
#include <stdlib.h>

#define NUM_FEATURES 	3
#define NUM_STATES 		4

#define MAX_T 			125

static const float EMISSION_MEAN[NUM_STATES][NUM_FEATURES] = {
	{0.0326109903915,0.020630839297,0.0202882445058},
	{1.33126739754,0.776312750875,0.259320918673},
	{0.250702987827,0.0691232981936,0.0313688631018},
	{0.159183724065,0.0717605041989,0.0290238469261}
};

static const float ONE_OVER_EMISSION_VARIANCE[NUM_STATES][NUM_FEATURES] = {
	{83282.7245443,397772.733019,1544502.26832},
	{0.995536568513,1.63997474215,16.1430353372},
	{122.307795585,744.972931237,9852.17391999},
	{55.2162956381,162.945903189,3230.59947146}
};

// ONE_OVER_SQRT_2PI / SQRT_EMISSION_VARIANCE
static const float NORMALISATION_FACTORS[NUM_STATES][NUM_FEATURES] = {
	{115.129741098,251.609810395,495.797509696},
	{0.398050958933,0.510891462796,1.6028861065},
	{4.41201657364,10.8888073027,39.5982598048},
	{2.96444706312,5.09250880697,22.6752260195}
};

//Log-normal distribution
//static const float EMISSION_MEAN[NUM_STATES][NUM_FEATURES] = {
//	{-3.4296960829,-3.8837800765,-3.89849536262},
//	{-0.0275821665726,-0.907044121826,-1.88173353303},
//	{-1.47975417718,-2.78658135341,-3.497468251},
//	{-2.12190075604,-3.13704555762,-3.68859599933}
//};
//static const float ONE_OVER_EMISSION_VARIANCE[NUM_STATES][NUM_FEATURES] = {
//	{83282.7245443,397772.733019,1544502.26832},
//	{0.995536568513,1.63997474215,16.1430353372},
//	{122.307795585,744.972931237,9852.17391999},
//	{55.2162956381,162.945903189,3230.59947146}
//};
//// ONE_OVER_SQRT_2PI / SQRT_EMISSION_VARIANCE
//static const float NORMALISATION_FACTORS[NUM_STATES][NUM_FEATURES] = {
//	{3.33773115632,5.38373460335,10.1036503433},
//	{0.476770554826,0.313636788388,0.365553648245},
//	{0.791608244174,0.852804605889,1.63400079729},
//	{0.557819024996,0.431614958011,0.777127525316}
//};

static const float TRANSITION_MATRIX[NUM_STATES][NUM_STATES] = {
	{0.60, 0.30, 0.05, 0.05},
	{0.10, 0.50, 0.30, 0.10},
	{0.15, 0.05, 0.40, 0.40},
	{0.40, 0.05, 0.15, 0.40}
};

static const float INITIAL[NUM_STATES] = {0.6f, 0.3f, 0.05f, 0.05f};

static float* data[3];

static float max_prob[NUM_STATES][MAX_T];
static int8_t edges[NUM_STATES][MAX_T];

static float col_max[NUM_STATES] = {0.0f};
static int8_t col_argmax[NUM_STATES] = {0};

static int8_t mpe[MAX_T] = {0};	//Most likely states
static float state_confidence[MAX_T] = {0.0f};	//Probabilities that the states chosen by the model are correct

static float emit[NUM_STATES];

#define FLT_MAX 3.40282e+38

float FastExp(float x) {
	float e = 1.0f + x * 0.00048828125f;	// 1/2048
	if(e > 1.04427f)	// 1.04427^(2^11) > FLT_MAX
		return FLT_MAX;
	if(e < -1.04427f)	// -1.04427^(2^11) ~= 0
		return 0.0;
	e *= e; e *= e; e *= e; e *= e;
	e *= e; e *= e; e *= e; e *= e;
	e *= e; e *= e; e *= e;
    return e;
}

//float lognormalpdf(float X, float mu, float p1, float one_over_variance) {
//	float mean_diff = log(X) - mu;
//	float pdf = p1 * FastExp((-1.0f * mean_diff * mean_diff) * 0.5f * one_over_variance);
//	return pdf;
//}

float normalpdf(float X, float mu, float p1, float one_over_variance) {
	float mean_diff = X - mu;
	float pdf = p1 * FastExp((-1.0f * mean_diff * mean_diff) * 0.5f * one_over_variance);
	return pdf;
}

float calculate(float freq1[], float freq2[], float freq3[], int T) {
	data[0] = freq1;
	data[1] = freq2;
	data[2] = freq3;

	if(T > MAX_T) {
		T = MAX_T;
	}

	for(int t=0; t<T; t++) {
		float max_emit = -1.0f;
		for(int i=0; i<NUM_STATES; i++) {
			float value = 1.0f;
			for(int j=0; j<NUM_FEATURES; j++) {
				value *= normalpdf(data[j][t], EMISSION_MEAN[i][j], NORMALISATION_FACTORS[i][j], ONE_OVER_EMISSION_VARIANCE[i][j]);
			}
			emit[i] = value;
			if(max_emit < value) {
				max_emit = value;
			}
		}

		max_emit *= 0.01f;	 // Normalise emissions so no one value is more than 100x greater than another
		for(int i=0; i<NUM_STATES; i++) {
			if(emit[i] < max_emit) {
				emit[i] = max_emit;
			}
		}

		if(t==0) {
			for(int k=0; k<NUM_STATES; k++) {
				max_prob[k][t] = INITIAL[k] * emit[k];
			}
		} else {
			for(int i=0; i<NUM_STATES; i++) {
				col_max[i] = 0.0f;
				col_argmax[i] = 0;
			}

			for(int i=0; i<NUM_STATES; i++) {
				for(int j=0; j<NUM_STATES; j++) {
					float product = max_prob[j][t-1] * TRANSITION_MATRIX[j][i] * emit[i];
					if(product > col_max[i]) {
						col_max[i] = product;
						col_argmax[i] = j;
					}
				}
			}

			for(int i=0; i<NUM_STATES; i++) {
				max_prob[i][t] = col_max[i];
				edges[i][t] = col_argmax[i];
			}
		}

		float max_prob_sum = 0.0f;
		for(int i=0; i<NUM_STATES; i++) {
			max_prob_sum += max_prob[i][t];
		}

		for(int i=0; i<NUM_STATES; i++) {
			max_prob[i][t] = max_prob[i][t] / max_prob_sum;
			if(isnan(max_prob[i][t])) {
				max_prob[i][t] = max_prob[i][t-1];
			}
		}
	}

	float current_max_prob = 0.0f;
	int8_t current_max_prob_arg = 0;
	for(int i=0; i<NUM_STATES; i++) {
		if(max_prob[i][T-1] > current_max_prob) {
			current_max_prob = max_prob[i][T-1];
			current_max_prob_arg = i;
		}
	}
	mpe[T-1] = current_max_prob_arg;
	state_confidence[T-1] = current_max_prob;

	for(int t=T-1; t>0; t--) {
		mpe[t-1] = edges[mpe[t]][t];
		state_confidence[t-1] = max_prob[mpe[t]][t];
	}

	float p_gunshot = 0.0f;
	for(int t=0; t<T; t++) {
		if(mpe[t] == 1 || mpe[t] == 2) {
//			p_gunshot += state_confidence[t];
			p_gunshot++;
		}
	}
	return p_gunshot;
}
