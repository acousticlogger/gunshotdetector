/*
 * main.c
 *
 *  Created on: 16 Jan 2017
 *      Author: Andrew Hill
 */
/****************************************************************************
 * main.c
 * Andrew Hill
 * University of Southampton
 * AudioMoth running on WonderGecko
 *****************************************************************************/

#include <stdbool.h>
#include <stdio.h>
#include <time.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_adc.h"
#include "em_prs.h"
#include "em_timer.h"
#include "em_int.h"
#include "em_usart.h"
#include "em_gpio.h"
#include "ff.h"
#include "microsd.h"
#include "diskio.h"
#include "em_opamp.h"
#include "em_usb.h"
#include "em_burtc.h"
#include "em_rmu.h"
#include "em_wdog.h"
#include "em_ebi.h"
#include "em_i2c.h"
#include "em_msc.h"

/* Recording file constants and WAV header structure */
#include "sdCard.h"

/* Handle USB interactions */
#include "usbcallbacks.h"
#include "usbdescriptors.h"

/* BURTC REGISTERS (Back up Real time counter)*/
#include "BURTC_registers.h"

#include "usb_requests.h"

#include "gunshot_detect.h"

/* Pinout defines */
#include "pinouts.h"

#include "nonvolatile_flash.h"
#include "configStore.h"

/* DAM interactions */
#include "em_dma.h"
#include "dmactrl.h"

/******************************************************************************
 * DC blocking filter Variables
 *****************************************************************************/
#define DC_BLOCKING_BITS                    9

volatile int32_t previousSample;
volatile int32_t previousFilterOutput;

/******************************************************************************
 * Defined constants, oscillator parameters
 *****************************************************************************/
#define LFXO_FREQUENCY                      32768
#define CLOCK_DIV                           128

/******************************************************************************
 * Buffer variables
 *****************************************************************************/
volatile int16_t* ramPointerAdcData[NUMBER_OF_BUFFERS];

volatile int16_t ramDMAPrim[ADC_DMA_SAMPLES];
volatile int16_t ramDMAAlt[ADC_DMA_SAMPLES];

volatile int sramAddress = 0;
volatile int primaryCounter = 0;
volatile int alternateCounter = 0;
volatile int writeBuffer = 0;
volatile int readBuffer1 = 0;
volatile int readBuffer2 = 0;
volatile bool twoBuffersReady;

int bufferSize = 0;
/******************************************************************************
 * Current recording file name
 *****************************************************************************/
char fileName[13];

/******************************************************************************
 * SD Card Variables
 *****************************************************************************/

static FATFS Fatfs;                     /* File system specific */
static FIL WAVfile;                     /* File to read WAV audio data from */
FRESULT res;                            /* Error results */
DSTATUS resCard;                        /* SDcard status */
UINT bw;                                /* File write count */

/******************************************************************************
 * Program flow variables
 *****************************************************************************/
typedef enum {USB_CONN_CHECK, TIME_CHECK, POWER_DOWN, INIT_MIC, LISTEN, RECORD} status_t;
status_t status = USB_CONN_CHECK;
status_t nextStatus = USB_CONN_CHECK;

uint32_t defaultConfig[CONFIG_STRUCT_LENGTH] = {	8000, 5, 5, 0, 0, 0, 0, 0, 0, 0, 64, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
							0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
							0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
							0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
							0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
							0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
							0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
							0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 	};

/******************************************************************************
 * Function prototypes
 *****************************************************************************/
void        setupLowPowCmu(void);                                                 /* Enable low power clocks */
void        setupHiPowCmu(void);                                                  /* Enable high power clocks */
void        setupBud(void);                                                       /* Setup back-up power domain */
void        setupBurtc(void);                                                     /* Setup RTC */
void        delay(uint16_t milliseconds);                                         /* Delay in 1/3 milliseconds */
int         switchCheck(void);                                                    /* Check for switch press */
int			initMic(void);														  /* Initialise microphone*/
int         record(void);                                                         /* Record sound to SD for 30 seconds unless button is pressed for 1 second */
int         listen(void);                                                         /* Configure TIMER and at a set sample rate - bat detector initialises record */
void        powerDown(void);                                                      /* Puts Device in EM4 Sleep mode until BURTC compare interrupt */
void        setupAdc(void);                                                       /* Sets up TIMER to trigger ADC through PRS */
void        setupI2C(void);
static void EbiConfigure(void);
static void EbiDisable(void);
void  		setLED(int pin, int value);                                           /* Set LED at pin if LEDs are enabled */
void        sdCardInitWav(void);                                                  /* Initialise SD card for Wav file */
void        sdCardWrite(const void* sampleToSave, int size);                      /* Sd card write */
void        sdCardClose(void);                                                    /* Sd card close */
DWORD       get_fattime(void);                                                    /* Get time for Fat file system */
void        gpioPinEnable(void);                                                  /* Enable used pins */
int         usbConnCheck(void);                                                   /* USB loop for USB callback function */
void 		setGainRequestFromUSB(uint32_t gain);
int         timeCheck(void);                                                      /* Check time and decide on next state */
bool        inTimeBlock(int hourOfDay, int minuteOfHour, int minHour, int minMinute, int maxHour, int maxMinute);
void        ADC0_IRQHandler(void);                                                /* Interrupt handler for ADC */


/******************************************************************************
 * Variable used to store the reset cause
 *****************************************************************************/
unsigned long resetCause;

/******************************************************************************
 * @brief  Main function
 *****************************************************************************/
int main(void) {
    /* Initialize chip */
    CHIP_Init();

    /* Turn on hard Floating point */
	SystemInit();

    /* Initialise counters and flags */
    previousSample = 0;
    previousFilterOutput = 0;
    writeBuffer = 0;
    readBuffer1 = 0;
    readBuffer2 = 0;
    sramAddress = 0;
    twoBuffersReady = false;

    status = USB_CONN_CHECK;
    nextStatus = USB_CONN_CHECK;

    /* Store the cause of the last reset, and clear the reset cause register */
    resetCause = RMU_ResetCauseGet();
    RMU_ResetCauseClear();

    /* Configuring clocks in the Clock Management Unit (CMU) */
	setupLowPowCmu();

	 /* Initial turn on initialise low frequency oscillator and set up BURTC*/
	if ((resetCause & RMU_RSTCAUSE_EM4WURST) == 0) {

		/* Start LFXO and wait until it is stable */
		CMU_OscillatorEnable(cmuOsc_LFXO, true, true);
		/* Setup up Back up domain EM4*/
		setupBud();
		/* Setup up Back up real time counter */
		setupBurtc();

		BURTC_RetRegSet(BUR_CLOCK_SET_FLAG, 0);
		BURTC_RetRegSet(BUR_BURT_COUNTER, 0);

		ConfigStore_writeData(defaultConfig);

	}

	//gpioPinEnable();

	nextStatus = switchCheck();

	while(1){

		status = nextStatus;

		switch(status) {
        case USB_CONN_CHECK:
            /* Check for usb connection */
            nextStatus = usbConnCheck();
            break;
        case TIME_CHECK:
            /* Check time is in correct period */
            nextStatus = timeCheck();
            break;
		case INIT_MIC:
			/* Power microphone and start ADC */
			nextStatus = initMic();
			break;
        case LISTEN:
        	nextStatus = listen();
        	break;
        case RECORD:
            /* Start recording to SD card */
            nextStatus = record();
            break;
		case POWER_DOWN:
			/* Power down into EM4 */
			powerDown();
			break;
		default:
			nextStatus = POWER_DOWN;
			break;

		}

	}
}

/******************************************************************************
 * @brief  Enable low energy clocks
 *****************************************************************************/
void setupLowPowCmu(void) {

    /* Enable clock to low energy modules */
    CMU_ClockEnable(cmuClock_CORELE, true);

    /* Enable High Frequency clocks */
    CMU_OscillatorEnable(cmuOsc_HFXO, true, true);
    CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO);
    CMU_ClockDivSet(cmuClock_HFPER, cmuClkDiv_1);

    CMU_ClockEnable(cmuClock_GPIO, true); 				/* GPIO clock */
    CMU_ClockEnable(cmuClock_TIMER1, true);             /* Delay TIMER */

}

/******************************************************************************
 * @brief  Enable High energy clocks
 *****************************************************************************/
void setupHiPowCmu(void) {

    CMU_ClockEnable(cmuClock_PRS, true);
    CMU_ClockEnable(cmuClock_TIMER3, true);             /* ADC TIMER */
    CMU_ClockEnable(cmuClock_ADC0, true);

}
/***************************************************************************
 * @brief Set up backup domain.
 ******************************************************************************/
void setupBud(void) {
    /* Initialize GPIO, BURTC and EM4 registers before going to sleep */
    EMU_EM4Init_TypeDef em4Init = EMU_EM4INIT_DEFAULT;

    em4Init.lockConfig    = true;                           /* Lock regulator, oscillator and BOD configuration.
                                                            * This needs to be set when using the
                                                            * voltage regulator in EM4 */
    em4Init.osc           = emuEM4Osc_LFXO;                 /* Select ULFRCO */
    em4Init.buRtcWakeup   = true;                           /* BURTC compare or overflow will generate reset */
    em4Init.vreg          = true;                           /* Enable voltage regulator. Needed for BURTC */

    /* Unlock configuration */
    EMU_EM4Lock( false );

    EMU_EM4Init( &em4Init );

    /* Enable access to BURTC registers */
    RMU_ResetControl(rmuResetBU, false);

    /* Lock configuration */
    EMU_EM4Lock( true );
}

/**************************************************************************//**
 * @brief  Configure RTC
 *****************************************************************************/
void setupBurtc(void) {
    /** Set up BURTC to count and trigger a wakeup in EM4 */
    BURTC_Init_TypeDef burtcInit = BURTC_INIT_DEFAULT;

    burtcInit.mode = burtcModeEM4;
    burtcInit.clkSel = burtcClkSelLFXO;
    burtcInit.clkDiv = burtcClkDiv_128;
    burtcInit.timeStamp = false;
    burtcInit.compare0Top = false;
    burtcInit.enable = false;
    burtcInit.lowPowerMode = burtcLPDisable;

    BURTC_Init(&burtcInit);

    BURTC_IntEnable( BURTC_IF_COMP0 );          /* Enable compare interrupt flag */

    BURTC_CompareSet(0, ((LFXO_FREQUENCY/CLOCK_DIV) * 2 ));   /* Set top value for comparator */

    BURTC_Enable(true);

}

/******************************************************************************
 * @brief Delay function
 *****************************************************************************/
void delay(uint16_t milliseconds) {
    /* Enable clock for TIMER1 */
    CMU->HFPERCLKEN0 |= CMU_HFPERCLKEN0_TIMER1;

    /* Set prescaler to maximum */
    TIMER1->CTRL = (TIMER1->CTRL & ~_TIMER_CTRL_PRESC_MASK) |  TIMER_CTRL_PRESC_DIV1024;

    /* Clear TIMER1 counter value */
    TIMER1->CNT = 0;

    /* Start TIMER1 */
    TIMER1->CMD = TIMER_CMD_START;

    /* Wait until counter value is over the threshold */
    while(TIMER1->CNT < 47*milliseconds){
    /* Do nothing, just wait */
    }

    /* Stop TIMER */
    TIMER1->CMD = TIMER_CMD_STOP;
}

/**************************************************************************//**
 * @brief  ADC Interrupt handler
 *****************************************************************************/
void ADC0_IRQHandler(void) {
    /* Clear ADC0 interrupt flag */
        ADC0->IFC = 1;

        int16_t newData;

        newData = ADC_DataSingleGet(ADC0);

        /* DC blocking filter */
        int32_t scaledPreviousFilterOutput = (previousFilterOutput * ((1 << DC_BLOCKING_BITS) - 1)) >> DC_BLOCKING_BITS;

        int32_t output = newData - previousSample + scaledPreviousFilterOutput;

        *(ramPointerAdcData[writeBuffer] + sramAddress) = (int16_t)output;

        previousFilterOutput = output;

        previousSample = (int32_t)newData;

        /* First check to see if first buffer is filled , on first check readBuffer1&2 and writeBuffer are always 0 */
        if (readBuffer1 == 0 && readBuffer2 == 0 && writeBuffer == 0) {
			if (primaryCounter == bufferSize-1){ /* When primary counter is one buffer size reset alternate counter and ram address*/
				writeBuffer = (writeBuffer + 1) % NUMBER_OF_BUFFERS; /* increment the buffer to write */
				alternateCounter = 0;
				sramAddress = 0;
			}
        }

        /* Check is primary counter is twice the buffer size if 8ksamples */
        if(primaryCounter == ((bufferSize*2)-1)) {

        	readBuffer1 = readBuffer2; /* re-assigning the first read Buffer to the last readBuffer to prepare for algorithm */
        	readBuffer2 = writeBuffer; /* assigning second read buffer to the current buffer that has just been filled */
        	writeBuffer = (writeBuffer + 1) % NUMBER_OF_BUFFERS;  /* increment the buffer to write */
            primaryCounter = 0;
            sramAddress = 0;
        	twoBuffersReady = true; /* set flag to start algorithm over ramPointerAdcData[readBuffer1] and ramPointerAdcData[readBuffer2] */

        } else if(alternateCounter == ((bufferSize*2)-1)){

        	readBuffer1 = readBuffer2; /* re-assigning the first read Buffer to the last readBuffer to prepare for algorithm */
        	readBuffer2 = writeBuffer; /* assigning second read buffer to the current buffer that has just been filled */
        	writeBuffer = (writeBuffer + 1) % NUMBER_OF_BUFFERS; /* increment the buffer to write */
        	alternateCounter = 0;
        	sramAddress = 0;
           	twoBuffersReady = true; /* set flag to start algorithm over ramPointerAdcData[readBuffer1] and ramPointerAdcData[readBuffer2] */

        } else {

        	/* incement counters and sramAddress */
        	sramAddress++;
        	primaryCounter++;
        	alternateCounter++;
        }

}

/**************************************************************************//**
 * @brief Configure TIMER to trigger ADC through PRS at a set sample rate
 *****************************************************************************/
void setupAdc(void) {

	ADC_Init_TypeDef adcInit = ADC_INIT_DEFAULT;
	/* Configure ADC single mode to sample Channel 0 */
	adcInit.timebase = ADC_TimebaseCalc(0);
	adcInit.prescale = ADC_PrescaleCalc(12000000, 0); /* Set highest allowed prescaler */
	adcInit.warmUpMode = adcWarmupNormal;
	//adcInit.lpfMode = adcLPFilterDeCap;

	adcInit.ovsRateSel = adcOvsRateSel64;

	ADC_Init(ADC0, &adcInit);

	ADC_InitSingle_TypeDef  adcInitSingle =
	{
		.prsSel     = adcPRSSELCh0,         /* ACD triggered by PRS channel 0 */
		.acqTime    = adcAcqTime1,          /* Acquisition time of 1 ADC clock cycle */
		.reference  = adcRefVDD,            /* Vdd as ADC reference */
		.resolution = adcResOVS,            /* 16 bit resolution */
		.input      = adcSingleInputCh0,    /* OpAmp as ADC input, result will be around 22445 with out decoupling cap un-amplified */
		.diff       = false,                /* differential input */
		.prsEnable  = true,                 /* PRS enable */
		.leftAdjust = false,                /* right adjust result */
		.rep        = false                 /* repetition */
	};
	ADC_InitSingle(ADC0, &adcInitSingle);

    /* Enable ADC interrupt when single conversion is complete */
    ADC0->IEN = ADC_IEN_SINGLE;

    /* Enable ADC interrupt vector in NVIC */
    NVIC_EnableIRQ(ADC0_IRQn);

}

/**************************************************************************//**
 * @brief  Setup I2C
 *****************************************************************************/
void setupI2C(void)
{

    I2C_Init_TypeDef i2cInit = I2C_INIT_DEFAULT;

    /* Using PC6 (SDA) and PC7 (SCL) */
    GPIO_PinModeSet(gpioPortC, 6, gpioModeWiredAndPullUpFilter, 1);
    GPIO_PinModeSet(gpioPortC, 7, gpioModeWiredAndPullUpFilter, 1);

    /* Enable pins at location 2 */
    I2C0->ROUTE = I2C_ROUTE_SDAPEN |
                    I2C_ROUTE_SCLPEN |
                    I2C_ROUTE_LOCATION_LOC2;

    /* Initializing the I2C */
    I2C_Init(I2C0, &i2cInit);

}

/**************************************************************************//**
 * @brief  Setup Mic
 *****************************************************************************/
int initMic(void) {

	bufferSize = SRAM_BUFFERSIZE;
	/* SRAM buffer pointer initialisation */
	ramPointerAdcData[0] = ((volatile int16_t*) 0x80000000);
	ramPointerAdcData[1] = ((volatile int16_t*) 0x80010000);
	ramPointerAdcData[2] = ((volatile int16_t*) 0x80020000);

	setupHiPowCmu();

	/* Turn SRAM card on*/
	GPIO_PinModeSet(SRAMEN_GPIOPORT, SRAM_ENABLE, gpioModePushPull, 0);

	EbiDisable();
	EbiConfigure();

	CMU_ClockEnable(cmuClock_I2C0, true);
	setupI2C();
	setGainRequestFromUSB(ConfigStore_getConfigWord(FLASH_GAIN));
	CMU_ClockEnable(cmuClock_I2C0, false);

    /* Configure ADC Sampling through PRS and setip I2C gain controller.
    *  Connect output of opAmp2 to ADC CH 0 AND VREF to ADC CH 1 */
    setupAdc();

	/* Enable Microphone */
	GPIO_PinModeSet(VMIC_GPIOPORT, VMIC_PIN, gpioModePushPull, 1);

    /* Connect PRS channel 0 to TIMER overflow */
    PRS_SourceSignalSet(0, PRS_CH_CTRL_SOURCESEL_TIMER3, PRS_CH_CTRL_SIGSEL_TIMER3OF, prsEdgeOff);
	/* Configure TIMER to trigger on sampling rate */
	TIMER_TopSet(TIMER3,  CMU_ClockFreqGet(cmuClock_TIMER3)/ConfigStore_getConfigWord(FLASH_SAMPLE_RATE));
	/* Enable Timer on ADC */
	TIMER_Enable(TIMER3, true);

    /* switch is in location 1 initiate recording */
    if(GPIO_PinInGet(SWITCH_GPIOPORT, SWITCH_PIN) == 0){
        return RECORD;
    } else if (ConfigStore_getConfigWord(FLASH_TRIGGER_ENABLED) == 1){

    	return LISTEN;

    } else {
    	return RECORD;
    }

}

/******************************************************************************
 * @brief Start recording, Wait for button press, Configure TIMER at a set sample rate
 *****************************************************************************/
int listen(void) {
	struct tm timePtr;
	time_t checkTime = getTimeRequestFromUSB();
	gmtime_r(&checkTime, &timePtr);

//	Check it is between the recording start and end date set by the config app
	if(((getTimeRequestFromUSB() >= ConfigStore_getConfigWord(FLASH_RECORDING_START_DATE)) && (getTimeRequestFromUSB() <= ConfigStore_getConfigWord(FLASH_RECORDING_END_DATE)) )
   		|| ((ConfigStore_getConfigWord(FLASH_RECORDING_START_DATE) == 0) && (ConfigStore_getConfigWord(FLASH_RECORDING_END_DATE) == 0)) || ((getTimeRequestFromUSB() >= ConfigStore_getConfigWord(FLASH_RECORDING_START_DATE)) && (ConfigStore_getConfigWord(FLASH_RECORDING_END_DATE) == 0))) {

//	   Iterate over the recording segments set by the config app
	   uint8_t numOfRecSegments = ConfigStore_getConfigWord(FLASH_NUMBER_OF_RECORDING_BLOCKS);
	   for(int i=0; i<numOfRecSegments; i++){

		   int counterIndex = 13+(i*4);	//In nonvolatile_flash.h, time block enums start at 13 and are in blocks of 4 (start hour, start min, etc.)

//			Check to see if the current time is within the block being checked
			bool inBlock = inTimeBlock(timePtr.tm_hour, timePtr.tm_min, ConfigStore_getConfigWord(counterIndex), ConfigStore_getConfigWord(counterIndex+1), ConfigStore_getConfigWord(counterIndex+2), ConfigStore_getConfigWord(counterIndex+3));

			if(inBlock) {
//				BURTC_RetRegSet(BUR_COUNTER_RESET_DAY, timePtr.tm_mday);

				while(true) {

					while(!twoBuffersReady) {
						EMU_EnterEM1();
					}
					twoBuffersReady = false;

					/* bat Detector Algorithm */
					int16_t* dataPointer1 = (int16_t*)ramPointerAdcData[readBuffer1];
					int16_t* dataPointer2 = (int16_t*)ramPointerAdcData[readBuffer2];

					int detectorState = gunshotDetector(dataPointer1, dataPointer2);

					//Exit the loop and start recording if this was the detection algorithm's response
					if(detectorState == RESPONSE_START_RECORDING) {
						/* Disable ADC interrupt vector in NVIC */
//						NVIC_DisableIRQ(ADC0_IRQn);
						return RECORD;
					} else if(detectorState == RESPONSE_CONTINUE_SAMPLING) {
						return LISTEN;
					}
				}
			}
		}

   }

   return POWER_DOWN;
}

static void EbiConfigure(void)
{
  /* Enable clocks */
  CMU_ClockEnable(cmuClock_EBI, true);
  CMU_ClockEnable(cmuClock_GPIO, true);

  EBI_Init_TypeDef ebiConfig = EBI_INIT_DEFAULT;

  /* Configure GPIO pins as push pull */
  /* EBI AD0..07 Data pins*/
  GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD00, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD01, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD02, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD03, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD04, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD05, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD06, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD07, gpioModePushPull, 0);

  /* EBI AD08..15 address pins*/
  GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD08, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD09, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD10, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD11, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD12, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD13, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD14, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD15,  gpioModePushPull, 0);

  /* EBI A16..24 extension address pins*/
  GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_A08, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_C, EBI_A09, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_C, EBI_A10, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_A11, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_A12, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_A13, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_A14, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_C, EBI_A15, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_B, EBI_A16, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_B, EBI_A17, gpioModePushPull, 0);

  /* EBI CS0-CS1 */
  GPIO_PinModeSet(EBI_GPIOPORT_D, EBI_CSEL1, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_D, EBI_CSEL2, gpioModePushPull, 0);

  /* EBI Wen/oen */
  GPIO_PinModeSet(EBI_GPIOPORT_F, EBI_OE, gpioModePushPull, 0);
  GPIO_PinModeSet(EBI_GPIOPORT_F, EBI_WE, gpioModePushPull, 0);

  /* Configure EBI controller, changing default values */
  ebiConfig.mode = ebiModeD8A8;
  ebiConfig.banks = EBI_BANK0;
  ebiConfig.csLines = EBI_CS0 | EBI_CS1;
  ebiConfig.readHalfRE = true;

  ebiConfig.aLow = ebiALowA8;
  ebiConfig.aHigh = ebiAHighA18;

  /* Address Setup and hold time */
  ebiConfig.addrHoldCycles  = 0;
  ebiConfig.addrSetupCycles = 0;

  /* Read cycle times */
  ebiConfig.readStrobeCycles = 3;
  ebiConfig.readHoldCycles   = 1;
  ebiConfig.readSetupCycles  = 2;

  /* Write cycle times */
  ebiConfig.writeStrobeCycles = 6;
  ebiConfig.writeHoldCycles   = 0;
  ebiConfig.writeSetupCycles  = 0;

  ebiConfig.location = ebiLocation1;
  /* Polarity values are default */

  /* Configure EBI */
  EBI_Init(&ebiConfig);

}

static void EbiDisable(void){

    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD00, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD01, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD02, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD03, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD04, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD05, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD06, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD07, gpioModeDisabled, 0);

    /* EBI AD08..15 address pins*/
    GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD08, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD09, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD10, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD11, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD12, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD13, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD14, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD15,  gpioModeDisabled, 0);

    /* EBI A16..24 extension address pins*/
    GPIO_PinModeSet(EBI_GPIOPORT_B, EBI_A16, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_B, EBI_A17, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_B, EBI_A18, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_B, EBI_A19, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_B, EBI_A21, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_B, EBI_A22, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_C, EBI_A23, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_C, EBI_A24, gpioModeDisabled, 0);

    /* EBI CS0-CS1 */
    GPIO_PinModeSet(EBI_GPIOPORT_D, EBI_CSEL1, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_D, EBI_CSEL2, gpioModeDisabled, 0);

    /* EBI Wen/oen */
    GPIO_PinModeSet(EBI_GPIOPORT_F, EBI_OE, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_F, EBI_WE, gpioModeDisabled, 0);

    /* Turn off EBI clock */
    CMU_ClockEnable(cmuClock_EBI, false);
}

/******************************************************************************
 * @brief  Check for switch press
 *****************************************************************************/
int switchCheck(void) {
    /* Configure PE9 as an input for switch with filter enable (out = 1)*/
    GPIO_PinModeSet(USB_GPIOPORT, USB_PIN, gpioModeInputPull, 0);
    GPIO_PinModeSet(SWITCH_GPIOPORT, SWITCH_PIN, gpioModeInputPull, 1);

    /* switch is in location 4 (AUTO) initiate recording */
    if((GPIO_PinInGet(SWITCH_GPIOPORT, SWITCH_PIN) == 1)&&(GPIO_PinInGet(USB_GPIOPORT, USB_PIN) == 0)){
    	return TIME_CHECK;
    } else if(GPIO_PinInGet(USB_GPIOPORT, USB_PIN) == 1){
    	return USB_CONN_CHECK;
    } else  {
    	return INIT_MIC;
    }
}

/**************************************************************************//**
 * @brief Start usb loop waiting for usb callbacks
 *****************************************************************************/
int usbConnCheck(void) {
    /* Set USB interrupt */
    GPIO_PinModeSet(USB_GPIOPORT, USB_PIN, gpioModeInput, 0);
    GPIO_PinModeSet(gpioPortF, 11, gpioModeInput, 0);

    if(GPIO_PinInGet(USB_GPIOPORT, USB_PIN)){

        /* Enable the USB interface */
        USBD_Init(&initstruct);

        while(GPIO_PinInGet(USB_GPIOPORT, USB_PIN)) {

        	if(GPIO_PinInGet(gpioPortF, 11)){

        		while(GPIO_PinInGet(USB_GPIOPORT, USB_PIN)) {
        				GPIO_PinModeSet(LED_GPIOPORT, GREEN_PIN, gpioModePushPull, 1);
        		}
        	    GPIO_PinModeSet(LED_GPIOPORT, GREEN_PIN, gpioModePushPull, 0);
        	}
        }

        /* Disable USB */
        USBD_AbortAllTransfers();
        USBD_Stop();
        USBD_Disconnect();
    }
    return TIME_CHECK;
}

/******************************************************************************
 * @brief Callback from the USB library to return the sample_rate
 *****************************************************************************/
void setGainRequestFromUSB(uint32_t gain) {

	uint8_t i2c_txBuffer[1];

	i2c_txBuffer[0] = gain;

	/* Transfer structure */
	I2C_TransferSeq_TypeDef i2cTransfer;

	/* Initializing I2C transfer */
	i2cTransfer.addr          = 0x5E;
	i2cTransfer.flags         = I2C_FLAG_WRITE;
	i2cTransfer.buf[0].data   = i2c_txBuffer;
	i2cTransfer.buf[0].len    = sizeof(i2c_txBuffer);

	I2C_TransferInit(I2C0, &i2cTransfer);

	/* Sending data */
	while (I2C_Transfer(I2C0) == i2cTransferInProgress){;}
}

/***************************************************************************
 * @brief Set up Time from USB
 ******************************************************************************/
int timeCheck(void) {
    struct tm timePtr;
    time_t checkTime = getTimeRequestFromUSB();
    gmtime_r(&checkTime, &timePtr);

     /* If day has changed since logger was last on, reset counter flag */
     if (BURTC_RetRegGet(BUR_COUNTER_RESET_DAY) != timePtr.tm_mday) {
        /* Reset day record counter BURTC register*/
        BURTC_RetRegSet(BUR_DAY_REC_COUNTER, 0);
        /* Set COUNTER_RESET_DAY BURTC register as today */
        BURTC_RetRegSet(BUR_COUNTER_RESET_DAY, timePtr.tm_mday);

        return POWER_DOWN;
     }

     if (/* BURTC_RetRegGet(DAY_REC_COUNTER) > DAY_RECORD_LIMIT || */BURTC_RetRegGet(BUR_CLOCK_SET_FLAG) == 0) {
        return POWER_DOWN;
    }

    if(((getTimeRequestFromUSB() >= ConfigStore_getConfigWord(FLASH_RECORDING_START_DATE)) && (getTimeRequestFromUSB() <= ConfigStore_getConfigWord(FLASH_RECORDING_END_DATE)) )
    		|| ((ConfigStore_getConfigWord(FLASH_RECORDING_START_DATE) == 0) && (ConfigStore_getConfigWord(FLASH_RECORDING_END_DATE) == 0)) || ((getTimeRequestFromUSB() >= ConfigStore_getConfigWord(FLASH_RECORDING_START_DATE)) && (ConfigStore_getConfigWord(FLASH_RECORDING_END_DATE) == 0))) {

		uint32_t numOfRecSegments = ConfigStore_getConfigWord(FLASH_NUMBER_OF_RECORDING_BLOCKS);

		for(int i = 0; i < numOfRecSegments; i++){

			int counterIndex = 13+(i*4);

			bool inBlock = inTimeBlock(timePtr.tm_hour, timePtr.tm_min, ConfigStore_getConfigWord(counterIndex), ConfigStore_getConfigWord(counterIndex+1), ConfigStore_getConfigWord(counterIndex+2), ConfigStore_getConfigWord(counterIndex+3));

			if(inBlock) {
				BURTC_RetRegSet(BUR_COUNTER_RESET_DAY, timePtr.tm_mday);
		    	return INIT_MIC;

			}
		}

    }

    return POWER_DOWN;
}

/***************************************************************************
 * @brief Is the given time between the two provided times?
 ******************************************************************************/
bool inTimeBlock(int hourOfDay, int minuteOfHour, int minHour, int minMinute, int maxHour, int maxMinute) {

    int currentMinutes = (hourOfDay * 60) + minuteOfHour;
    int lowerBoundMinutes = (minHour * 60) + minMinute;
    int upperBoundMinutes = (maxHour * 60) + maxMinute;


    if ( (lowerBoundMinutes < upperBoundMinutes) &&  (currentMinutes >= lowerBoundMinutes && currentMinutes <= upperBoundMinutes) ){
        return true;
    }

    if ( (lowerBoundMinutes > upperBoundMinutes) && (currentMinutes >= lowerBoundMinutes || currentMinutes <= upperBoundMinutes) ){
        if (currentMinutes <= lowerBoundMinutes && currentMinutes <= upperBoundMinutes) {
            return true;
        }

        if (currentMinutes >= lowerBoundMinutes && currentMinutes >= upperBoundMinutes) {
            return true;
        }
    }

    return false;
}

/******************************************************************************
 * @brief Save recording to SD card
 *****************************************************************************/
int record(void) {

    /* Enter EM1 while ADC transfer is active to save power. Note that
    * interrupts are disabled to prevent the ISR from being triggered
    * after checking the twoBuffersReady flag, but before entering
    * sleep. If this were to happen, there would be no interrupt to wake
    * the core again and the MCU would be stuck in EM1. While the
    * core is in sleep, pending interrupts will still wake up the
    * core and the ISR will be triggered after interrupts are enabled
    * again.
    */

    GPIO_PinModeSet(SDEN_GPIOPORT, SD_ENABLE, gpioModePushPull, 0);

    /* Configure SD card */
    sdCardInitWav();

	setLED(RED_PIN, 1);
	sdCardWrite((const void*)ramPointerAdcData[readBuffer1], (bufferSize*2));
	sdCardWrite((const void*)ramPointerAdcData[readBuffer2], (bufferSize*2));

	setLED(RED_PIN, 0);

    /* Close SD Card */
    sdCardClose();
    GPIO_PinModeSet(SDEN_GPIOPORT, SD_ENABLE, gpioModePushPull, 1);

    /* Check switch position is not in MAN  */
	if((GPIO_PinInGet(SWITCH_GPIOPORT, SWITCH_PIN) == 1)||(GPIO_PinInGet(USB_GPIOPORT, USB_PIN) == 1)) {
		 BURTC_RetRegSet(BUR_DAY_REC_COUNTER, (BURTC_RetRegGet(BUR_DAY_REC_COUNTER) + 1));
		return POWER_DOWN;
	}

	return POWER_DOWN;

}


/******************************************************************************
 * @brief Initialise SD card
 *****************************************************************************/
void sdCardInitWav(void) {
    /** WAV header structure */
    bool n = false;

    wavHeader.frequency = SAMPLE_RATE;
    wavHeader.bytes_per_second = SAMPLE_RATE * 2;
    wavHeader.totallength = ((SAMPLE_RATE*2*RECORD_TIME_SEC) + sizeof(wavHeader))  - 8;
    wavHeader.bytes_in_data = FILE_SIZE - sizeof(wavHeader);

    int timeFile = getTimeRequestFromUSB();

    sprintf(fileName, "%08x.WAV", timeFile);

    while(1) {
        MICROSD_Init();                     /* Initialise MicroSD driver */

        resCard = disk_initialize(0);       /*Check SD card status */

        switch(resCard) {
            case STA_NOINIT:                    /* Drive not initialised */
                if (n == false) {

                	setLED(RED_PIN, 1);
					delay(1000);

                    n = true;
                }
                break;
            case STA_NODISK:                    /* No medium in the drive */
                if (n == false) {
                	setLED(RED_PIN, 1);
					delay(1000);
                    n = true;
                }
                break;
            case STA_PROTECT:                   /* Write protected */
                if (n == false) { /* Drive not initialised */
                	setLED(RED_PIN, 1);
					delay(1000);
                    n = true;
                }
                break;
            default:
                break;
        }

        if (!resCard) break;                /* Drive initialised. */
    }

    /* Mount the file system */

    /* Initialise filesystem */
    if (f_mount(0, &Fatfs) != FR_OK) {
        /* Error - No SD with FAT32 is present */
    	setLED(RED_PIN, 1);
        delay(1000);
    }

    res = f_open(&WAVfile, fileName,  FA_WRITE);
    if (res != FR_OK) {
        /*  If file does not exist, create it*/
        res = f_open(&WAVfile, fileName, FA_CREATE_ALWAYS | FA_WRITE );
        if (res != FR_OK) {

        	setLED(RED_PIN, 1);
            delay(1000);
        }
    }

    /*Set the file write pointer to first location */
    res = f_lseek(&WAVfile, 0);

    if (res != FR_OK) {
    	setLED(RED_PIN, 1);
        delay(1000);
    }

    /* Create WAV file header */
    res = f_write(&WAVfile, &wavHeader, sizeof(WAV_Header_TypeDef), &bw);

    if ((res != FR_OK) || (sizeof(WAV_Header_TypeDef) != bw)) {
    	setLED(RED_PIN, 1);
        delay(1000);
    }
}


/******************************************************************************
 * @brief Write to SD card
 *****************************************************************************/
void sdCardWrite(const void* sampleToSave, int size) {
    /* Create WAV file data */
    res = f_write(&WAVfile, sampleToSave, size, &bw);

    if ((res != FR_OK) || ( size != bw)) {
        /* Error. Cannot write the file */
    	setLED(RED_PIN, 1);
        delay(1000);
    }
}

/******************************************************************************
 * @brief Finish writing on SD card and close file
 *****************************************************************************/
void sdCardClose(void) {
    /* Close the file */
    res = f_close(&WAVfile);

    if (res != FR_OK) {
    	setLED(RED_PIN, 1);
        delay(1000);
    }
}

/*******************************************************************************
 * @brief This function is required by the FAT file system in order to provide
 * timestamps for created files.
 * Refer to reptile/fatfs/doc/en/fattime.html for the format of this DWORD.
 *
 * @return	A DWORD containing the current time and date as a packed datastructure.
 ******************************************************************************/
DWORD get_fattime(void) {
    time_t fatTime = getTimeRequestFromUSB();
    struct tm timePtr;
    gmtime_r(&fatTime, &timePtr);

    return (((unsigned int)timePtr.tm_year - 208) << 25) |
            (((unsigned int)timePtr.tm_mon + 1 ) << 21) |
            ((unsigned int)timePtr.tm_mday << 16) |
            ((unsigned int)timePtr.tm_hour << 11) |
            ((unsigned int)timePtr.tm_min << 5) |
            ((unsigned int)timePtr.tm_sec >> 1);
}

/**************************************************************************//**
 * @brief Power Down Function
 *****************************************************************************/
void powerDown(void) {

    int rtcSleepLength = (int)ConfigStore_getConfigWord(FLASH_SLEEP_LENGTH);
    int rtcCountToWakeUp = (int)((LFXO_FREQUENCY/CLOCK_DIV) * rtcSleepLength);

    if ((BURTC_RetRegGet(BUR_CLOCK_SET_FLAG) != 1)) {
    	/* Structs for modules used */
    	setLED(RED_PIN, 1);
    	setLED(GREEN_PIN, 1);
    	delay(10);
    	setLED(RED_PIN, 0);
    	setLED(GREEN_PIN, 0);
    } else {
    	setLED(GREEN_PIN, 1);
    	delay(10);
    	setLED(GREEN_PIN, 1);
    }

	//gpioPinEnable();

	/* CLear BURTC comparison flag and set new comparison value*/
	BURTC_IntClear( BURTC_IF_COMP0 );


    BURTC_CompareSet(BUR_BURT_COUNTER, BURTC_CounterGet() + rtcCountToWakeUp);

	/* Enter EM4 */
	EMU_EnterEM4();

}

/******************************************************************************
 * @brief  Set LED at pin if LEDs are enabled
 *****************************************************************************/
void setLED(int pin, int value) {
	 GPIO_PinModeSet(LED_GPIOPORT, pin, gpioModePushPull, value);
	if(ConfigStore_getConfigWord(FLASH_LED_ENABLED)) {
        GPIO_PinModeSet(LED_GPIOPORT, pin, gpioModePushPull, value);
    }
}

/******************************************************************************
 * @brief  Disable unused GPIO pins Enable ued pins
 *****************************************************************************/
void gpioPinEnable(void) {
    /* GPIO A */
    GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD09, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD10, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD11, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD12, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD13, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD14, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD15, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortA, 7, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortA, 8, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortA, 9, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortA, 10, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortA, 11, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortA, 12, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortA, 13, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortA, 14, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_A, EBI_AD08, gpioModeDisabled, 0);

    /* GPIO B */
    GPIO_PinModeSet(EBI_GPIOPORT_B, EBI_A16, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_B, EBI_A17, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortB, 2, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortB, 3, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortB, 4, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortB, 5, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortB, 6, gpioModeDisabled, 0);

    GPIO_PinModeSet(gpioPortB, 9, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortB, 10, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortB, 11, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortB, 12, gpioModeDisabled, 0);


    /* GPIO C */
    GPIO_PinModeSet(gpioPortC, 0, gpioModeDisabled, 0); /* ACMP battery voltage */
    GPIO_PinModeSet(gpioPortC, 1, gpioModeDisabled, 0);
    //GPIO_PinModeSet(SRAMEN_GPIOPORT, SRAM_ENABLE, gpioModeWiredAndPullUp, 1);
    //GPIO_PinModeSet(SDEN_GPIOPORT, SD_ENABLE, gpioModeWiredAndPullUp, 1);
    //GPIO_PinModeSet(LED_GPIOPORT, RED_PIN, gpioModeDisabled, 0);
    //GPIO_PinModeSet(LED_GPIOPORT, GREEN_PIN, gpioModePushPull, 0);
    //GPIO_PinModeSet(gpioPortC, 6, gpioModeDisabled, 0); /* SDA */
    //GPIO_PinModeSet(gpioPortC, 7, gpioModeDisabled, 0); /* SCL */
    GPIO_PinModeSet(EBI_GPIOPORT_C, EBI_A15, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_C, EBI_A09, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_C, EBI_A10, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortC, 11, gpioModeDisabled, 0);

    /* GPIO D */
    /* GPIO_PinModeSet(gpioPortD, 0, gpioModeDisabled, 0); //Mic input */
    GPIO_PinModeSet(gpioPortD, 1, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortD, 2, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortD, 3, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortD, 4, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortD, 5, gpioModeDisabled, 0);
    GPIO_PinModeSet(VMIC_GPIOPORT, VMIC_PIN, gpioModeDisabled, 0);
    //GPIO_PinModeSet(SWITCH_GPIOPORT, SWITCH_PIN, gpioModeDisabled, 0);
    //GPIO_PinModeSet(USB_GPIOPORT, USB_PIN, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_D, EBI_CSEL1, gpioModeWiredAndPullUp, 1);
    GPIO_PinModeSet(EBI_GPIOPORT_D, EBI_CSEL2, gpioModePushPull, 0);
    GPIO_PinModeSet(gpioPortD, 11, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortD, 12, gpioModeDisabled, 0);

    /* GPIO E */
    GPIO_PinModeSet(gpioPortE, 0, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_A08, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortE, 2, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortE, 3, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_A11, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_A12, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_A13, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_A14, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD00, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD01, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD02, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD03, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD04, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD05, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD06, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_E, EBI_AD07, gpioModeDisabled, 0);

    /* GPIO F */
//    GPIO_PinModeSet(gpioPortF, 0, gpioModePushPull, 0); /* DEBUG: SWCLK */
//    GPIO_PinModeSet(gpioPortF, 1, gpioModePushPull, 0); /* DEBUG: SWDIO */
//    GPIO_PinModeSet(gpioPortF, 2, gpioModePushPull, 0); /* DEBUG: SWO */
    GPIO_PinModeSet(gpioPortF, 3, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortF, 4, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortF, 5, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortF, 6, gpioModeDisabled, 0);
    GPIO_PinModeSet(gpioPortF, 7, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_F, EBI_WE, gpioModeDisabled, 0);
    GPIO_PinModeSet(EBI_GPIOPORT_F, EBI_OE, gpioModeDisabled, 0);
    //GPIO_PinModeSet(gpioPortF, 10, gpioModePushPull, 0); /* USB D- */
    //GPIO_PinModeSet(gpioPortF, 11, gpioModePushPull, 0); /* USB D+ */
    GPIO_PinModeSet(gpioPortF, 12, gpioModeDisabled, 0); /* USB ID */

    GPIO->CTRL = GPIO_CTRL_EM4RET;
}

