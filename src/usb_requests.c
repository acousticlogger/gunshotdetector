/****************************************************************************
 * usb_requests.c
 * Peter Prince
 * University of Southampton
 *****************************************************************************/

#include "usb_requests.h"

/******************************************************************************
 * @brief Callback from the USB library to return the time
 *****************************************************************************/
uint32_t getTimeRequestFromUSB() {
    return BURTC_RetRegGet(BUR_BURT_COUNTER) + ((BURTC_CounterGet() + (1 << 7)) >> 8);
}

/******************************************************************************
 * @brief Callback from the USB library to set the time
 *****************************************************************************/
void setTimeRequestFromUSB(uint32_t timeNow) {
    BURTC_RetRegSet(BUR_BURT_COUNTER, timeNow);
    BURTC_RetRegSet(BUR_CLOCK_SET_FLAG, 1);
    BURTC_RetRegSet(BUR_WATCH_DOG_FLAG, 0);

    /* Reset day record counter BURTC register*/
    BURTC_RetRegSet(BUR_DAY_REC_COUNTER, 0);

    BURTC_CounterReset();
}

/******************************************************************************
 * @brief Callback from the USB library to return the sample_rate
 *****************************************************************************/
void setRequestFromUSB(int burRegister, uint32_t burValue) {
	BURTC_RetRegSet(burRegister, burValue);
}

