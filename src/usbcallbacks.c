/****************************************************************************
 * callbacks.c
 * Alex Rogers
 * University of Oxford
 *****************************************************************************/

#include "em_usb.h"
#include "em_rtc.h"

#include "usbcallbacks.h"
#include "usbdescriptors.h"
#include "configStore.h"

/* BURTC REGISTERS (Back up Real time counter)*/
#include "BURTC_registers.h"

#define CONFIG_CAPACITY 		1024
#define CONFIG_STRUCT_LENGTH 	(CONFIG_CAPACITY/4)

#define BUFFERSIZE 64

/* Buffers for received and transmitted data */

STATIC_UBUF(receiveBuffer, BUFFERSIZE);
STATIC_UBUF(transmitBuffer, BUFFERSIZE);
uint32_t ramBuffer[CONFIG_STRUCT_LENGTH];

/* Definition of two callbacks which the application must provide */

extern uint32_t getTimeRequestFromUSB(void);

extern void setTimeRequestFromUSB(uint32_t);

/* Callback which provides the HID specific descriptors */

int setupCmd( const USB_Setup_TypeDef *setup ) {
	int retVal = USB_STATUS_REQ_UNHANDLED;

	if (setup->Type == USB_SETUP_TYPE_STANDARD) {
		if (setup->bRequest == GET_DESCRIPTOR) {
			switch (setup->wValue >> 8) {
				case USB_HID_REPORT_DESCRIPTOR:
					USBD_Write( 0, (void*)HID_ReportDescriptor, EFM32_MIN(sizeof(HID_ReportDescriptor), setup->wLength), NULL );
					retVal = USB_STATUS_OK;
					break;

				case USB_HID_DESCRIPTOR:
					USBD_Write( 0, (void*)HID_Descriptor, EFM32_MIN(sizeof(HID_Descriptor), setup->wLength), NULL );
					retVal = USB_STATUS_OK;
					break;
			}
		}
	}

	return retVal;
}

/* Callback to start the USB reading process when the device is configured */

void stateChange(USBD_State_TypeDef oldState, USBD_State_TypeDef newState) {
	if (newState == USBD_STATE_CONFIGURED) {
		USBD_Read(EP_OUT, receiveBuffer, BUFFERSIZE, dataReceivedCallback);
	}
}

/* Callback on completion of data send. Used to request next read */

int dataSentCallback(USB_Status_TypeDef status, uint32_t xferred, uint32_t remaining) {
	USBD_Read(EP_OUT, receiveBuffer, BUFFERSIZE, dataReceivedCallback);
	return USB_STATUS_OK;
}

/* Callback on receipt of message from the USB host. Sets and gets the current time */

int dataReceivedCallback(USB_Status_TypeDef status, uint32_t xferred, uint32_t remaining) {
	if (receiveBuffer[0] == 0x01) {
		/* Requests the current time from the device */
		uint32_t timeNow = getTimeRequestFromUSB();

		transmitBuffer[0] = (timeNow >> 24) & 255;
		transmitBuffer[1] = (timeNow >> 16) & 255;
		transmitBuffer[2] = (timeNow >> 8) & 255;
		transmitBuffer[3] = timeNow & 255;

		USBD_Write(EP_IN, transmitBuffer, 64, dataSentCallback);

	} else if (receiveBuffer[0] == 0x02) {
		/* Provides the time to set the device RTC */

		uint32_t timeNow = ((uint32_t)receiveBuffer[1] << 24) + ((uint32_t)receiveBuffer[2] << 16) + ((uint32_t)receiveBuffer[3] << 8) + receiveBuffer[4];

		setTimeRequestFromUSB(timeNow);

		USBD_Read(EP_OUT, receiveBuffer, BUFFERSIZE, dataReceivedCallback);

	}  else if (receiveBuffer[0] == 0x03) {
		uint32_t *unique_0 = (uint32_t *) 0xFE081F0;
		uint32_t *unique_1 = (uint32_t *) 0xFE081F4;

		transmitBuffer[0] = (*unique_0 >> 24) & 255;
		transmitBuffer[1] = (*unique_0 >> 16) & 255;
		transmitBuffer[2] = (*unique_0 >> 8) & 255;
		transmitBuffer[3] = *unique_0 & 255;

		transmitBuffer[4] = (*unique_1 >> 24) & 255;
		transmitBuffer[5] = (*unique_1 >> 16) & 255;
		transmitBuffer[6] = (*unique_1 >> 8) & 255;
		transmitBuffer[7] = *unique_1 & 255;

		USBD_Write(EP_IN, transmitBuffer, 64, dataSentCallback);

	} else if (receiveBuffer[0] == 0x04) {
        /* Configures logger settings */

		uint32_t timeNow = ((uint32_t)receiveBuffer[1] << 24) + ((uint32_t)receiveBuffer[2] << 16) + ((uint32_t)receiveBuffer[3] << 8) + receiveBuffer[4];

		setTimeRequestFromUSB(timeNow);

		USBD_Read(EP_OUT, receiveBuffer, BUFFERSIZE, dataReceivedCallback);

		ramBuffer[FLASH_SAMPLE_RATE] = ((uint32_t)receiveBuffer[5] << 16) + ((uint32_t)receiveBuffer[6] << 8) + receiveBuffer[7];

		ramBuffer[FLASH_RECORDING_LENGTH] = ((uint32_t)receiveBuffer[8] << 16) + ((uint32_t)receiveBuffer[9] << 8) + receiveBuffer[10];

		ramBuffer[FLASH_SLEEP_LENGTH] = ((uint32_t)receiveBuffer[11] << 16) + ((uint32_t)receiveBuffer[12] << 8) + receiveBuffer[13];

		ramBuffer[FLASH_LISTEN_LENGTH] = ((uint32_t)receiveBuffer[14] << 16) + ((uint32_t)receiveBuffer[15] << 8) + receiveBuffer[16];

		ramBuffer[FLASH_RECORDING_START_DATE] = ((uint32_t)receiveBuffer[17] << 24) + ((uint32_t)receiveBuffer[18] << 16) + ((uint32_t)receiveBuffer[19] << 8) + receiveBuffer[20];
		ramBuffer[FLASH_RECORDING_END_DATE] = ((uint32_t)receiveBuffer[21] << 24) + ((uint32_t)receiveBuffer[22] << 16) + ((uint32_t)receiveBuffer[23] << 8) + receiveBuffer[24];

		ramBuffer[FLASH_GAIN]  = (uint32_t)receiveBuffer[25];

		ramBuffer[FLASH_LED_ENABLED] = (uint32_t)receiveBuffer[26];

		ramBuffer[FLASH_NUMBER_OF_RECORDING_BLOCKS] = (uint32_t)receiveBuffer[27];

		for(int i=0; i<ramBuffer[FLASH_NUMBER_OF_RECORDING_BLOCKS]; i++) {

			int counterIndex = 28+(i*4);

			switch(i){
			case 0:
				ramBuffer[FLASH_RECORDING_START_TIME_H_1] = (uint32_t)receiveBuffer[counterIndex];
				ramBuffer[FLASH_RECORDING_START_TIME_M_1] = (uint32_t)receiveBuffer[counterIndex+1];
				ramBuffer[FLASH_RECORDING_END_TIME_H_1] = (uint32_t)receiveBuffer[counterIndex+2];
				ramBuffer[FLASH_RECORDING_END_TIME_M_1] = (uint32_t)receiveBuffer[counterIndex+3];

				break;
			case 1:
				ramBuffer[FLASH_RECORDING_START_TIME_H_2] = (uint32_t)receiveBuffer[counterIndex];
				ramBuffer[FLASH_RECORDING_START_TIME_M_2] = (uint32_t)receiveBuffer[counterIndex+1];
				ramBuffer[FLASH_RECORDING_END_TIME_H_2] = (uint32_t)receiveBuffer[counterIndex+2];
				ramBuffer[FLASH_RECORDING_END_TIME_M_2] = (uint32_t)receiveBuffer[counterIndex+3];
				break;
			case 2:
				ramBuffer[FLASH_RECORDING_START_TIME_H_3] = (uint32_t)receiveBuffer[counterIndex];
				ramBuffer[FLASH_RECORDING_START_TIME_M_3] = (uint32_t)receiveBuffer[counterIndex+1];
				ramBuffer[FLASH_RECORDING_END_TIME_H_3] = (uint32_t)receiveBuffer[counterIndex+2];
				ramBuffer[FLASH_RECORDING_END_TIME_M_3] = (uint32_t)receiveBuffer[counterIndex+3];
				break;
			case 3:
				ramBuffer[FLASH_RECORDING_START_TIME_H_4] = (uint32_t)receiveBuffer[counterIndex];
				ramBuffer[FLASH_RECORDING_START_TIME_M_4] = (uint32_t)receiveBuffer[counterIndex+1];
				ramBuffer[FLASH_RECORDING_END_TIME_H_4] = (uint32_t)receiveBuffer[counterIndex+2];
				ramBuffer[FLASH_RECORDING_END_TIME_M_4] = (uint32_t)receiveBuffer[counterIndex+3];
				break;
			case 4:

				ramBuffer[FLASH_RECORDING_START_TIME_H_5] = (uint32_t)receiveBuffer[counterIndex];
				ramBuffer[FLASH_RECORDING_START_TIME_M_5] = (uint32_t)receiveBuffer[counterIndex+1];
				ramBuffer[FLASH_RECORDING_END_TIME_H_5] = (uint32_t)receiveBuffer[counterIndex+2];
				ramBuffer[FLASH_RECORDING_END_TIME_M_5] = (uint32_t)receiveBuffer[counterIndex+3];
				break;
			default:
				break;

			}

		}

		USBD_Read(EP_OUT, receiveBuffer, BUFFERSIZE, dataReceivedCallback);

    } else if (receiveBuffer[0] == 0x05) {
        /* Configures logger's trigger settings */
//    	ramBuffer[FLASH_TRIGGER_ENABLED] = (uint32_t)receiveBuffer[1];
//    	ramBuffer[FLASH_TRIGGER_FREQUENCY] = ((uint32_t)receiveBuffer[2] << 16) + ((uint32_t)receiveBuffer[3] << 8) + receiveBuffer[4];
//    	ramBuffer[FLASH_TRIGGER_BANDWIDTH] = ((uint32_t)receiveBuffer[5] << 16) + ((uint32_t)receiveBuffer[6] << 8) + receiveBuffer[7];
//    	ramBuffer[FLASH_TRIGGER_THRESHOLD] = ((uint32_t)receiveBuffer[8] << 16) + ((uint32_t)receiveBuffer[9] << 8) + receiveBuffer[10];

        USBD_Read(EP_OUT, receiveBuffer, BUFFERSIZE, dataReceivedCallback);
    }

	ConfigStore_writeData(ramBuffer);

	return USB_STATUS_OK;
}
